
//
//  MenuViewController.swift
//  TAD_Demo
//
//  Created by Taslima Roya on 6/27/20.
//  Copyright © 2020 Taslima Roya. All rights reserved.
//

import UIKit


struct MenuInfo {
    var menuTitle: UIImage
    var isSelected: Bool
    
    init(withTitle TitleImage: UIImage, isSelected selected: Bool) {
        self.menuTitle = TitleImage
        self.isSelected = selected
    }
}

class MenuViewController: UIViewController {
    
    
    @IBOutlet weak var menuCollectionView: UICollectionView!
    
    var menuList = [#imageLiteral(resourceName: "firstBar"),#imageLiteral(resourceName: "secondBar"),#imageLiteral(resourceName: "thirdBar"),#imageLiteral(resourceName: "fourthBar")]
    var segueIdentifiers = ["First", "Second", "Third", "Fourth"]
    var menuDataSource: [MenuInfo] = []
    var masterVc: MasterViewController?
    var currentIndex: Int = 0

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupDataSource()
        self.initialSetup()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "container" {
            self.masterVc = segue.destination as? MasterViewController
        }
    }
    
    @IBAction func handleRightSwipe(_ sender: UISwipeGestureRecognizer) {
        if currentIndex > 0 {
            self.currentIndex = self.currentIndex - 1
            let indexPath = IndexPath(row: currentIndex, section: 0)
            self.menuCollectionView.scrollToItem(at: indexPath, at: .left, animated: true)
            
            self.updateMenuObjects()
            self.masterVc?.segueIdentifierReceivedFromParent(identifier: self.segueIdentifiers[self.currentIndex])
        }
    }
    
    @IBAction func handleLeftSwipe(_ sender: Any) {
        if currentIndex < self.segueIdentifiers.count - 1 {
            self.currentIndex = self.currentIndex + 1
            let indexPath = IndexPath(row: currentIndex, section: 0)
            
            self.menuCollectionView.scrollToItem(at: indexPath, at: .left, animated: true)
            self.updateMenuObjects()
            self.masterVc?.segueIdentifierReceivedFromParent(identifier: self.segueIdentifiers[self.currentIndex])
        }
    }
}

extension MenuViewController {
    
    //Creates a data source array for Horizontal Menu with tapped state.
    func setupDataSource() {
        for (index, menuItem) in self.menuList.enumerated() {
            if index == 0 {
                let menuObject = MenuInfo(withTitle: menuItem, isSelected: true)
                self.menuDataSource.append(menuObject)
            } else {
                let menuObject = MenuInfo(withTitle: menuItem, isSelected: false)
                self.menuDataSource.append(menuObject)
            }
        }
    }

    //Controls Initial Navigation upon App Launch.
    func initialSetup() {
        masterVc?.segueIdentifierReceivedFromParent(identifier: self.segueIdentifiers[0])
    }
    
    //Updates Menu List Objects upon - Tap on Menu or Scroll using swipe Gestures
    func updateMenuObjects() {
        let indexPath = IndexPath(row: currentIndex, section: 0)
        for (index, _) in self.menuDataSource.enumerated() {
            if index == indexPath.row {
                self.menuDataSource[index].isSelected = true
            } else {
                self.menuDataSource[index].isSelected = false
            }
        }
        self.menuCollectionView.reloadData()
    }
}

// Collection View Data Source Methods:
extension MenuViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.menuDataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MenuCell", for: indexPath) as? CustomMenuCell
        let menuObject = self.menuDataSource[indexPath.row]
       cell?.menuTiltleImage.image = menuObject.menuTitle
        
        if menuObject.isSelected {
            cell?.menuTitleView.backgroundColor = #colorLiteral(red: 0.8901960784, green: 0.1568627451, blue: 0.2784313725, alpha: 1)
            
            cell?.menuTiltleImage.tintColor = #colorLiteral(red: 0.8901960784, green: 0.1568627451, blue: 0.2784313725, alpha: 1)
           cell?.menuTiltleImage.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        } else {
            cell?.menuTitleView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            cell?.menuTiltleImage.tintColor = #colorLiteral(red: 0.4392156863, green: 0.4392156863, blue: 0.4392156863, alpha: 1)
            cell?.menuTiltleImage.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }
        return cell!
    }
}

//Collection View Delegate Methods
extension MenuViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.currentIndex = indexPath.row
        collectionView.scrollToItem(at: indexPath, at: .left, animated: true)
        for (index, _) in self.menuDataSource.enumerated() {
            if index == indexPath.row {
                self.menuDataSource[index].isSelected = true
            } else {
                self.menuDataSource[index].isSelected = false
            }
        }
        collectionView.reloadData()
        masterVc?.segueIdentifierReceivedFromParent(identifier: self.segueIdentifiers[indexPath.row])
    }
}

