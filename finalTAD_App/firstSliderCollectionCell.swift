//
//  firstSliderCollectionCell.swift
//  TAD_Demo
//
//  Created by Taslima Roya on 6/29/20.
//  Copyright © 2020 Taslima Roya. All rights reserved.
//

import UIKit

class firstSliderCollectionCell: UICollectionViewCell {
    
    @IBOutlet var imageOutletOfSlider: UIImageView!
    @IBOutlet var labelOutletOfSlider: UILabel!
    
    
}
