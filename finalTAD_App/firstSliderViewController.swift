//
//  firstSliderViewController.swift
//  TAD_Demo
//
//  Created by Taslima Roya on 6/29/20.
//  Copyright © 2020 Taslima Roya. All rights reserved.
//

import UIKit

class firstSliderViewController: UIViewController {
    
    var labelData = ["Send Money","Mobile Recharge","Make Payment Merchant","Cash Out","Add Money"]
    var id = [String]()
    
    var imageData = [#imageLiteral(resourceName: "ic_2_Artboard 1"),#imageLiteral(resourceName: "Group 92"), #imageLiteral(resourceName: "ic_1_Artboard 1"),#imageLiteral(resourceName: "ic_8_Artboard 1"),#imageLiteral(resourceName: "ic_7_Artboard 1")]


    @IBOutlet var firstSliderOutlet: UICollectionView!
    //var imageData = [UIImage]()
    override func viewDidLoad() {
        super.viewDidLoad()
        id = ["A"]
        
        
    }

}

extension firstSliderViewController:UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return labelData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "sliderCell", for: indexPath) as! firstSliderCollectionCell
        cell.imageOutletOfSlider.image = imageData[indexPath.row]
        cell.labelOutletOfSlider.text = labelData[indexPath.row]
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let name = id[indexPath.row]
        let viewController = storyboard?.instantiateViewController(withIdentifier: name)
        self.navigationController?.pushViewController(viewController!, animated: true)
    }
    
    
    
}

extension firstSliderViewController:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionWidth = collectionView.bounds.width
        return CGSize(width: collectionWidth/3, height: collectionWidth/3)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
