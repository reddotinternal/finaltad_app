//
//  firstSliderViewController.swift
//  TAD_Demo
//
//  Created by Taslima Roya on 6/29/20.
//  Copyright © 2020 Taslima Roya. All rights reserved.
//

import UIKit

class secondSliderViewController: UIViewController {
    //var label = UILabel()
    
    var labelData = ["Insurance","Bills & Payment","Salary Disbursement","Deposit into Saving A/C","Recruitment Fees", "Passport/NID Fees","Tution Fees","Foreign Remittance"]
    var imageData = [#imageLiteral(resourceName: "firstCollection"),#imageLiteral(resourceName: "secondCollection"), #imageLiteral(resourceName: "thirdCollection"),#imageLiteral(resourceName: "fourthCollection"),#imageLiteral(resourceName: "fifthCollection"), #imageLiteral(resourceName: "sixthCollection"), #imageLiteral(resourceName: "7thCollection"), #imageLiteral(resourceName: "8thCollection")]

    @IBOutlet var secondCollectionViewOutlet: UICollectionView!
    
    //var imageData = [UIImage]()
    override func viewDidLoad() {
        super.viewDidLoad()
//
//        label.frame = CGRect(x:5, y:100,width:80,height: 100)
//
//        label.text = "Do more"
//        label.textAlignment = .center
//        label.textColor = UIColor.black
//        label.backgroundColor = UIColor.white
//        self.view.addSubview(label)
    }

}

extension secondSliderViewController:UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return labelData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "secondSliderCell", for: indexPath) as! secondSliderCollectionCell
        cell.secondSliderOutletImage.image = imageData[indexPath.row]
        cell.secondSliderOutlet.text = labelData[indexPath.row]
        
        return cell
    }
    
    
    
}

extension secondSliderViewController:UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionWidth = collectionView.bounds.width
        return CGSize(width: collectionWidth/3, height: collectionWidth/3)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
