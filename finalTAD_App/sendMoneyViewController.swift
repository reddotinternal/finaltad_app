//
//  sendMoneyViewController.swift
//  finalTAD_App
//
//  Created by Taslima Roya on 7/9/20.
//  Copyright © 2020 Taslima Roya. All rights reserved.
//

import UIKit
import UIKit

@IBDesignable
class sendMoneyShadow: UIView {
    //Shadow
    @IBInspectable var shadowColor: UIColor = UIColor.black {
        didSet {
            self.updateView()
        }
    }
    @IBInspectable var shadowOpacity: Float = 0.5 {
        didSet {
            self.updateView()
        }
    }
    @IBInspectable var shadowOffset: CGSize = CGSize(width: 3, height: 3) {
        didSet {
            self.updateView()
        }
    }
    @IBInspectable var shadowRadius: CGFloat = 15.0 {
        didSet {
            self.updateView()
        }
    }

    //Apply params
    func updateView() {
        self.layer.shadowColor = self.shadowColor.cgColor
        self.layer.shadowOpacity = self.shadowOpacity
        self.layer.shadowOffset = self.shadowOffset
        self.layer.shadowRadius = self.shadowRadius
    }
}

class sendMoneyViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        
    }
    

}
